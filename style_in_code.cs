Style style = new Style(typeof(TextBlock));

style.Setters.Add(new Setter(TextBlock.FontSizeProperty, 96));

style.Setters.Add(new Setter(TextBlock.FontFamilyProperty,new FontFamily("Times New Roman")));